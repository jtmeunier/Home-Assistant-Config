# [Home Assistant](https://home-assistant.io/) Configurations

My configuration files for the open-source [Home Assistant](https://home-assistant.io/) home-automation platform. Provided as a reference, with no warranty or other support.

## Why

When I was getting started with Home Assistant, I benefitted greatly from the configuration examples provided in their [Cookbook](https://home-assistant.io/cookbook/).

## Using

With the exception of some sensitive data, these configurations are taken verbatim from the source repository that tracks my Raspberry Pi's configuration. Several omissions should be corrected, including API password, latitutde and longitude, and other sensitive settings.

